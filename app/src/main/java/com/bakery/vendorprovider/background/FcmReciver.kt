package com.bakery.vendorprovider.background

import com.bakery.vendorprovider.rxbus.RxBusNotification
import com.bakery.vendorprovider.utils.SharedHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FcmReciver : FirebaseMessagingService() {


    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)


        RxBusNotification.send("")
    }
    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        SharedHelper(this).firebaseToken = p0
    }
}