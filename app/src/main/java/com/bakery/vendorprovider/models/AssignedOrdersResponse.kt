package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AssignedOrdersResponse : Serializable {


    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("orderDetails")
    var orderDetails: AssignedOrderData? = null


}

class AssignedOrderData : Serializable {

    @SerializedName("storeName")
    var storeName = ""

    @SerializedName("storeLat")
    var storeLat = ""

    @SerializedName("storeLng")
    var storeLng = ""

    @SerializedName("storeAddress")
    var storeAddress = ""

    @SerializedName("userName")
    var userName = ""

    @SerializedName("userLat")
    var userLat = ""

    @SerializedName("userLng")
    var userLng = ""

    @SerializedName("userAddress")
    var userAddress = ""

    @SerializedName("orderRefferenceId")
    var orderRefferenceId = ""

    @SerializedName("orderId")
    var orderId = 0

    @SerializedName("quantity")
    var quantity = ""

    @SerializedName("amount")
    var amount = ""


    @SerializedName("orderStatus")
    var orderStatus = ""

}

