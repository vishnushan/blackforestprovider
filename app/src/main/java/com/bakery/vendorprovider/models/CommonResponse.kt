package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CommonResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""
}