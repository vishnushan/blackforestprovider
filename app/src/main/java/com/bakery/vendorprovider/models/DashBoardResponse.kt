package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DashBoardResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("details")
    var details: DashBoardData? = null

}

class DashBoardData : Serializable {

    @SerializedName("todayEarning")
    var todayEarning: String? = ""

    @SerializedName("todayOrders")
    var todayOrders: String? = ""

    @SerializedName("yesterdayEarnings")
    var yesterdayEarnings: String? = ""

    @SerializedName("yesterdayOrders")
    var yesterdayOrders: String? = ""

    @SerializedName("weekEarnings")
    var weekEarnings: String? = ""

    @SerializedName("weekOrderd")
    var weekOrderd: String? = ""

    @SerializedName("isOnline")
    var isOnline: Boolean? = null
}