package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OrderDetailsResponse : Serializable {


    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("orderDetails")
    var orderDetails: OrderDetailsData? = null


}

class OrderDetailsData : Serializable {

    @SerializedName("storeName")
    var storeName = ""

    @SerializedName("storeLat")
    var storeLat = ""

    @SerializedName("storeLng")
    var storeLng = ""

    @SerializedName("storeAddress")
    var storeAddress = ""

    @SerializedName("userName")
    var userName = ""

    @SerializedName("mobileNumber")
    var mobileNumber = ""

    @SerializedName("userLat")
    var userLat = ""

    @SerializedName("userLng")
    var userLng = ""

    @SerializedName("userAddress")
    var userAddress = ""

    @SerializedName("orderRefferenceId")
    var orderRefferenceId = ""

    @SerializedName("orderId")
    var orderId = 0

    @SerializedName("quantity")
    var quantity = ""

    @SerializedName("amount")
    var amount = ""

    @SerializedName("orderStatus")
    var orderStatus = ""

    @SerializedName("orderProductDetails")
    var orderProductDetails: ArrayList<OrderedProductDetails>? = null


}

class OrderedProductDetails : Serializable {

    @SerializedName("id")
    var id = 0

    @SerializedName("orderId")
    var orderId = 0

    @SerializedName("productId")
    var productId = 0

    @SerializedName("productName")
    var productName = ""

    @SerializedName("quantity")
    var quantity = ""

    @SerializedName("price")
    var price = ""

    @SerializedName("withEgg")
    var withEgg: String? = ""

    @SerializedName("weight")
    var weight = ""

    @SerializedName("messageOnCake")
    var messageOnCake = ""

    @SerializedName("created_At")
    var created_At = ""

}
