package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class OrderListResponse : Serializable {


    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("orderList")
    var orderList: ArrayList<OrderListData>? = null
}

class OrderListData : Serializable {


    @SerializedName("id")
    var id = 0

    @SerializedName("refference_Id")
    var refference_Id = ""

    @SerializedName("lat")
    var lat = ""

    @SerializedName("long")
    var long = ""

    @SerializedName("address")
    var address = ""

    @SerializedName("tag")
    var tag: String? = null

    @SerializedName("quantity")
    var quantity = ""

    @SerializedName("total")
    var total = ""

    @SerializedName("order_status")
    var order_status = ""

    @SerializedName("userName")
    var userName = ""

    @SerializedName("mobileNumber")
    var mobileNumber = ""

}