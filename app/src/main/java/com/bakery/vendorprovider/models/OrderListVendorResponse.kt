package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OrderListVendorResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("orderlist")
    var orderlist = ArrayList<OrderListVendorData>()

}

class OrderListVendorData : Serializable {

    @SerializedName("id")
    var id = ""

    @SerializedName("userId")
    var userId = ""

    @SerializedName("refference_Id")
    var refference_Id = ""

    @SerializedName("quantity")
    var quantity = ""

    @SerializedName("total")
    var total = ""

    @SerializedName("order_status")
    var order_status = ""

    @SerializedName("updated_At")
    var updated_At = ""

    @SerializedName("address")
    var address = ""

    @SerializedName("productId")
    var productId = ""

    @SerializedName("productName")
    var productName = ""

    @SerializedName("price")
    var price = ""

    @SerializedName("withEgg")
    var withEgg = ""

}
