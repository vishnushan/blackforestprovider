package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductDetailResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("productdetails")
    var productList: ArrayList<ProductDetail>? = null

}

class ProductDetail : Serializable {


    @SerializedName("outletId")
    var outletId = 0

    @SerializedName("name")
    var name = ""

    @SerializedName("quantity")
    var quantity = 0

    @SerializedName("productId")
    var productId = ""

    @SerializedName("price")
    var price = ""

    @SerializedName("ImageUrl")
    var ImageUrl = ""

    @SerializedName("description")
    var description = ""

    @SerializedName("status")
    var status = 0


}


