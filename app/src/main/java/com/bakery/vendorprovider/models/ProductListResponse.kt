package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductListResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("productList")
    var productList = ArrayList<ProductList>()

}

class ProductList : Serializable {


    @SerializedName("outletId")
    var outletId = 0

    @SerializedName("name")
    var name = ""


    @SerializedName("productId")
    var productId = 0


    @SerializedName("ImageUrl")
    var ImageUrl = ""

    @SerializedName("status")
    var status = 0


}
