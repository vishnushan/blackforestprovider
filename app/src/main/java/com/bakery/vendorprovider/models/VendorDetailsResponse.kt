package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VendorDetailsResponse : Serializable {


    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""


    @SerializedName("outletDetails")
    var providerDetails: VendorDetailsData? = null

    @SerializedName("yarnings")
    var yarnings: YarningsData? = null


}


class VendorDetailsData : Serializable {

    @SerializedName("id")
    var id = "0"

    @SerializedName("email")
    var email = ""

    @SerializedName("name")
    var name = ""

    @SerializedName("image")
    var image = ""

    @SerializedName("addressLineOne")
    var addressLineOne = ""

    @SerializedName("latitude")
    var latitude = ""

    @SerializedName("longitude")
    var longitude = ""

    @SerializedName("status")
    var status = ""


}

class YarningsData : Serializable {

    @SerializedName("todayEarning")
    var todayEarning = ""

    @SerializedName("todayOrders")
    var todayOrders = ""

    @SerializedName("yesterdayEarnings")
    var yesterdayEarnings = ""

    @SerializedName("yesterdayOrders")
    var yesterdayOrders = ""

    @SerializedName("weekEarnings")
    var weekEarnings = ""

    @SerializedName("weekOrderd")
    var weekOrderd = ""

}
