package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VerifyOtpResponse : Serializable {


    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""

    @SerializedName("accesstoken")
    var accesstoken = ""

    @SerializedName("providerDetails")
    var providerDetails: ProviderDetails? = null


}

class ProviderDetails : Serializable {


    @SerializedName("id")
    var id = ""

    @SerializedName("FirstName")
    var FirstName = ""

    @SerializedName("LastName")
    var LastName = ""

    @SerializedName("MobileNumber")
    var MobileNumber = ""

    @SerializedName("StreetAddress")
    var StreetAddress = ""

    @SerializedName("SocialSecurityNumber")
    var SocialSecurityNumber = ""

    @SerializedName("EmailAddress")
    var EmailAddress = ""

    @SerializedName("PostalCode")
    var PostalCode = ""

    @SerializedName("State")
    var State = ""

    @SerializedName("City")
    var City = ""

    @SerializedName("vehicleType")
    var vehicleType = ""

    @SerializedName("latitude")
    var latitude = ""

    @SerializedName("longitude")
    var longitude = ""


}