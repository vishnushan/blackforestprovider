package com.bakery.vendorprovider.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VerifyOtpVendorResponse : Serializable {


    @SerializedName("error")
    var error = false

    @SerializedName("msg")
    var msg = ""


    @SerializedName("oulletDetails")
    var providerDetails: VendorDetails? = null


}

class VendorDetails : Serializable{

    @SerializedName("id")
    var id = "0"

  @SerializedName("email")
    var email = ""

  @SerializedName("name")
    var name = ""

  @SerializedName("image")
    var image = ""

  @SerializedName("addressLineOne")
    var addressLineOne = ""

  @SerializedName("latitude")
    var latitude = ""

 @SerializedName("longitude")
    var longitude = ""


}