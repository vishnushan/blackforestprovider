package com.bakery.vendorprovider.network

import android.app.Activity
import android.content.Context
import android.os.StrictMode
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.background.AppController
import com.bakery.vendorprovider.utils.Coroutien

import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.utils.isNetworkConnected
import com.bakery.vendorprovider.utils.openNewTaskActivity
import com.bakery.vendorprovider.view.activity.LoginActivity

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody


import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit


object Api {

    var MY_SOCKET_TIMEOUT_MS = 50000
    var TAG = Api::class.java.simpleName

    fun postMethod(input: ApiInput, apiResponseCallback: ApiResponseCallback) {
        Log.d("$TAG Request", "${input.url.toString()} ${input.jsonObject.toString()}")
        if (input.context?.isNetworkConnected()!!) {

            var jsonObjectRequest =
                object : JsonObjectRequest(Request.Method.POST, input.url, input.jsonObject, {
                    apiResponseCallback.setResponseSuccess(it)
                    Log.d("$TAG response", "${input.url.toString()} ${it.toString()}")
                }, {
                    if (it is TimeoutError || it is NoConnectionError) {
                        input.context?.getString(R.string.no_internet_connection)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is AuthFailureError) {
                        moveToLoginActivity(input.context)
//                    input.context?.getString(R.string.session_expired)?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ServerError) {
                        input.context?.getString(R.string.server_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is NetworkError) {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ParseError) {
                        input.context?.getString(R.string.parsing_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    }
                }) {
                    override fun getHeaders(): MutableMap<String, String> {
                        return if (input.headers != null) {
                            val params: HashMap<String, String> = HashMap<String, String>()

                            for ((key, value) in input.headers!!) {
                                params[key] = value
                            }
                            params
                        } else {
                            super.getHeaders()
                        }
                    }
                }
            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )

            Log.d("$TAG Request", input.toString())

            AppController.getInstance().addrequestToQueue(jsonObjectRequest)
        } else {
            apiResponseCallback.setErrorResponse("No Internet Connection")
        }
    }

    private fun moveToLoginActivity(context: Context?) {

        context?.let {

            SharedHelper(it).loggedIn = false
            SharedHelper(it).token = ""

            (it as Activity).openNewTaskActivity(LoginActivity::class.java)
        }
    }

    fun getMethod(input: ApiInput, apiResponseCallback: ApiResponseCallback) {
        Log.d("$TAG Request", "${input.url.toString()} ${input.headers.toString()}")
        if (input.context?.isNetworkConnected()!!) {

            val jsonObjectRequest =
                object : JsonObjectRequest(Request.Method.GET, input.url, input.jsonObject, {
                    apiResponseCallback.setResponseSuccess(it)
                    Log.d("$TAG Response", "${input.url.toString()} ${it.toString()}")
                }, {

                    if (it is TimeoutError || it is NoConnectionError) {
                        input.context?.getString(R.string.no_internet_connection)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is AuthFailureError) {
                        moveToLoginActivity(input.context)
//                        input.context?.getString(R.string.session_expired)
//                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ServerError) {
                        input.context?.getString(R.string.server_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is NetworkError) {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ParseError) {
                        input.context?.getString(R.string.parsing_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    }
                }) {
                    override fun getHeaders(): MutableMap<String, String> {
                        return if (input.headers != null) {
                            val params: HashMap<String, String> = HashMap<String, String>()

                            for ((key, value) in input.headers!!) {
                                params[key] = value
                            }
                            params
                        } else {
                            super.getHeaders()
                        }
                    }
                }

            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )

            Log.d("$TAG Request", input.toString())


            AppController.getInstance().addrequestToQueue(jsonObjectRequest)
        } else {
            apiResponseCallback.setErrorResponse("No Internet Connection")
        }
    }

    fun uploadImage(
        file: File,
        url: String,
        context: Context,
        apiResponseCallback: ApiResponseCallback
    ) {
        if (context.isNetworkConnected()) {

            val MEDIA_TYPE_PNG = MediaType.parse("image/jpeg")

            val requestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "file",
                    "fileName",
                    RequestBody.create(MEDIA_TYPE_PNG, File(file.path))
                )
                .build()
            val request = okhttp3.Request.Builder()
                .url(url)
                .post(requestBody).build()
            Log.e(TAG, "file: " + file.path)

            Coroutien.iOWorker {
                try {
                    var response = uploadRequest(request)
                    val jsonObject = JSONObject(response.body()!!.string())
                    apiResponseCallback.setResponseSuccess(jsonObject)
                } catch (e: IOException) {
                    Log.e(TAG, "IOException: $e")
                    apiResponseCallback.setErrorResponse(e.message.toString())
                } catch (e: JSONException) {
                    apiResponseCallback.setErrorResponse(e.message.toString())
                }
            }

        } else {
            apiResponseCallback.setErrorResponse("No Internet Connection")
        }
    }

    fun uploadRequest(request: okhttp3.Request): okhttp3.Response {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        val client = builder.build()
        val response = client.newCall(request).execute()
        return response
    }


}