package com.bakery.vendorprovider.network

import org.json.JSONObject

interface ApiResponseCallback {
    fun setResponseSuccess(jsonObject: JSONObject)
    fun setErrorResponse(error : String)
}