package com.bakery.vendorprovider.network

import com.bakery.vendorprovider.BuildConfig
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.background.AppController


object UrlHelper {
    private const val BASE = BuildConfig.Base
    const val SOCKETBASEURL = BuildConfig.SocketURL
    private const val BASE_URL = "$BASE/deliveryboy/"
    private const val OUTLET_BASE_URL = "$BASE/outlet/"


    const val GET_OTP = BASE_URL + "getOtp"
    const val VERIFY_OTP = BASE_URL + "verify"
    const val DASHBOARD = BASE_URL + "dashboard"
    const val UPDATEONLINESTATUS = BASE_URL + "updateOnlineStatus"
    const val GETASSIGNORDERS = BASE_URL + "getassignorder"
    const val DELIVERYORDERDETAILS = BASE_URL + "deliveryOrderDetails"
    const val UPDATEDEVICETOKEN = BASE_URL + "updatedeviceToken"

    const val UPDATEORDER_STATUS = BASE_URL + "updateorderstatus"
    const val PASTORDERLIST = BASE_URL + "listPastOrders"
    const val UPDATEPROFILE = BASE_URL + "updateProfile"


    //vendor
    const val LOGIN = OUTLET_BASE_URL + "login"
    const val VERIFY_OTP_VENDOR = OUTLET_BASE_URL + "verifyOtp"
    const val UPDATE_DEVICE_TOKEN_VENDOR = OUTLET_BASE_URL + "updateDeviceToken"
    const val GETOUTLETDETAILS = OUTLET_BASE_URL + "getOutletDetails"
    const val UPDATESTATUS = OUTLET_BASE_URL + "updateStatus"
    const val VENDORPRODUCTLIST = OUTLET_BASE_URL + "vendor_product_list"
    const val UPDATEPRODUCTSTATUS = OUTLET_BASE_URL + "updateProductStatus"

    const val PRODUCTDETAILS = OUTLET_BASE_URL + "productDetails"
    const val UPDATEPRODUCTS = OUTLET_BASE_URL + "updateProduct"
    const val ORDERLIST = OUTLET_BASE_URL + "orderList"
    const val UPDATEPRDERSTATUS = OUTLET_BASE_URL + "updateOrderStatus"

    const val UPDATELOCATION =  "updateLocation"


    const val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
    const val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
    const val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
    const val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
    const val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"
    const val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"


    fun getAddress(
        latitude: Double,
        longitude: Double
    ): String? {
        val lat = latitude.toString()
        val lngg = longitude.toString()
        return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
                + lngg + "&sensor=true&key=" + AppController.getInstance().resources.getString(
            R.string.map_api_key
        ))
    }
}