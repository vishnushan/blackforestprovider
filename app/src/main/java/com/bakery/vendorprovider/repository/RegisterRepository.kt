package com.bakery.vendorprovider.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bakery.vendorprovider.models.*
import com.bakery.vendorprovider.network.Api
import com.bakery.vendorprovider.network.ApiInput
import com.bakery.vendorprovider.network.ApiResponseCallback
import com.google.gson.Gson
import org.json.JSONObject

class RegisterRepository private constructor() {


    companion object {
        private var repository: RegisterRepository? = null

        fun getInstance(): RegisterRepository {
            if (repository == null) {
                repository = RegisterRepository()
            }
            return repository as RegisterRepository
        }
    }

//    fun checkMobileexist(apiParams: ApiInput): LiveData<CheckMobileExistResponse> {
//
//        val apiResponse: MutableLiveData<CheckMobileExistResponse> = MutableLiveData()
//
//        Api.postMethod(apiParams, object : ApiResponseCallback {
//            override fun setResponseSuccess(jsonObject: JSONObject) {
//                val gson = Gson()
//                val response: CheckMobileExistResponse =
//                    gson.fromJson(jsonObject.toString(), CheckMobileExistResponse::class.java)
//                apiResponse.value = response
//            }
//
//            override fun setErrorResponse(error: String) {
//                val response = CheckMobileExistResponse()
//                response.error = true
//                response.msg = error
//                apiResponse.value = response
//            }
//        })
//
//        return apiResponse
//
//    }

    fun getOtp(apiParams: ApiInput): LiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun verifyOtp(apiParams: ApiInput): LiveData<VerifyOtpResponse> {

        val apiResponse: MutableLiveData<VerifyOtpResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: VerifyOtpResponse =
                    gson.fromJson(jsonObject.toString(), VerifyOtpResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = VerifyOtpResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getDashBoardData(apiParams: ApiInput): MutableLiveData<DashBoardResponse> {

        val apiResponse: MutableLiveData<DashBoardResponse> = MutableLiveData()

        Api.getMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: DashBoardResponse =
                    gson.fromJson(jsonObject.toString(), DashBoardResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = DashBoardResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun switchonlineStatus(apiParams: ApiInput): MutableLiveData<CommonResponse> {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getAssignedOrders(apiParams: ApiInput): MutableLiveData<AssignedOrdersResponse> {

        val apiResponse: MutableLiveData<AssignedOrdersResponse> = MutableLiveData()

        Api.getMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: AssignedOrdersResponse =
                    gson.fromJson(jsonObject.toString(), AssignedOrdersResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = AssignedOrdersResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getOrderDetails(apiParams: ApiInput): MutableLiveData<OrderDetailsResponse> {
        val apiResponse: MutableLiveData<OrderDetailsResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: OrderDetailsResponse =
                    gson.fromJson(jsonObject.toString(), OrderDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = OrderDetailsResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateStatus(apiParams: ApiInput): MutableLiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getList(apiParams: ApiInput): MutableLiveData<OrderListResponse> {

        val apiResponse: MutableLiveData<OrderListResponse> = MutableLiveData()

        Api.getMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: OrderListResponse =
                    gson.fromJson(jsonObject.toString(), OrderListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = OrderListResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun updateProfile(apiParams: ApiInput): MutableLiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun verifyOtpVendor(apiParams: ApiInput): LiveData<VerifyOtpVendorResponse> {
        val apiResponse: MutableLiveData<VerifyOtpVendorResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: VerifyOtpVendorResponse =
                    gson.fromJson(jsonObject.toString(), VerifyOtpVendorResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = VerifyOtpVendorResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateDeviceToken(apiParams: ApiInput) {
        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {}
            override fun setErrorResponse(error: String) {}
        })

    }

    fun getDetails(apiParams: ApiInput): MutableLiveData<VendorDetailsResponse> {

        val apiResponse: MutableLiveData<VendorDetailsResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: VendorDetailsResponse =
                    gson.fromJson(jsonObject.toString(), VendorDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = VendorDetailsResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateVendorStatus(apiParams: ApiInput): MutableLiveData<CommonResponse> {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


    fun updateDeviceTokenDelivery(apiParams: ApiInput) {


        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {}
            override fun setErrorResponse(error: String) {}
        })
    }

    fun getProductList(apiParams: ApiInput): MutableLiveData<ProductListResponse> {


        val apiResponse: MutableLiveData<ProductListResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ProductListResponse =
                    gson.fromJson(jsonObject.toString(), ProductListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = ProductListResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateProductStatus(apiParams: ApiInput): MutableLiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse


    }

    fun getProductDetails(apiParams: ApiInput): MutableLiveData<ProductDetailResponse> {

        val apiResponse: MutableLiveData<ProductDetailResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ProductDetailResponse =
                    gson.fromJson(jsonObject.toString(), ProductDetailResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = ProductDetailResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateProductDetails(apiParams: ApiInput): MutableLiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getOrders(apiParams: ApiInput): MutableLiveData<OrderListVendorResponse> {
        val apiResponse: MutableLiveData<OrderListVendorResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: OrderListVendorResponse =
                    gson.fromJson(jsonObject.toString(), OrderListVendorResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = OrderListVendorResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateOrderAccepted(apiParams: ApiInput): MutableLiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateOrderRejected(apiParams: ApiInput): MutableLiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.msg = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


}