package com.bakery.vendorprovider.utils

import android.Manifest


object Constants {

    object ApiKeys {

        const val AUTHORIZATION = "Authorization"
        const val LANG = "lang"

        const val MOBILE_NUMBER = "mobileNumber"
        const val OTP = "otp"

        const val NAME = "name"
        const val PASSWORD = "password"

        const val LAT = "lat"
        const val LNG = "lng"
        const val FORMATEDADDRESS = "formataddress"
        const val ADDRESS1 = "address1"
        const val LANDMARK = "landmark"
        const val TYPE = "type"
        const val ADDRESSID = "addressId"

        const val CATEGORYID = "categoryId"
        const val FLAVOURID = "flavourId"

        const val PRODUCTID = "productId"
        const val STATUS = "status"
        const val ORDERID = "orderId"


    }

    object IntentKeys {
        const val MOBILE_NUMNER = "mobilenumber"
        const val ADDRESSID = "addressId"

        const val ISCATEGORY = "isCategory"
        const val NAME = "name"
        const val ID = "id"

        const val FORGOTPASSWORDFLOW = "forgotPasswordFlow"
        const val ORDERID = "orderId"

    }


    object Permission {

        const val READ_STORAGE_PERMISSIONS = 202
        const val CAMERA_STORAGE_PERMISSIONS = 203
        const val READ_FILE_PERMISSIONS = 204
        const val READ_AUDIO_PERMISSIONS = 205
        const val VIDEO_CALL_PERMISSION = 206
        const val AUDIO_CALL_PERMISSION = 207
        const val LOCATION_PERMISSION = 208

        val CAMERA_PERM_LIST = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val READ_STORAGE_PERM_LIST = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        val AUDIO_CALL_PERMISSION_LIST = arrayOf(Manifest.permission.RECORD_AUDIO)
        val VIDEO_CALL_PERMISSION_LIST =
            arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA)

        val LOCATION_PERMISSION_PERMISSON_LIST = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    }

    object RequestCode {


        const val CAMERA_INTENT = 101
        const val GALLERY_INTENT = 102
        const val FILE_INTENT = 103
        const val AUDIO_INTENT = 104
        const val GPS_REQUEST = 105

    }


    //image Upload from amazon s3
    object AWS {
//        const val POOL_ID: String = BuildConfig.POOL_ID
//        const val BASE_S3_URL: String = BuildConfig.BASE_S3_URL
//        const val ENDPOINT: String = BuildConfig.ENDPOINT
//        const val BUCKET_NAME: String = BuildConfig.BUCKET_NAME
//        val REGION = Regions.AP_SOUTH_1
    }

    object OrderStatus {
        const val ASSIGNED = "assigned"
        const val ACCEPTED = "accepted"
        const val REJECTED = "rejected"
        const val PICKEDUP = "pickedup"
        const val DELIVERED = "delivered"
    }

    object SocketKey {

        const val ID = "staffId"
        const val LATUTUDE = "latitude"
        const val LONGITUDE = "longitude"
    }


}