package com.bakery.vendorprovider.utils

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*


fun String.longDateToDisplayTimeString(showDateForOlderTime: Boolean = false): String {
    try {
        val date = Date(this.toLong())
        return if (showDateForOlderTime) {
            when {
                date.isToday() -> {
                    val format = SimpleDateFormat("hh:mm a", Locale.getDefault())
                    format.format(date)
                }
                date.isYesterday() -> {
                    "yesterday"
                }
                else -> {
                    val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                    format.format(date)
                }
            }

        } else {
            val format = SimpleDateFormat("hh:mm a", Locale.getDefault())
            format.format(date)
        }
    } catch (e: Exception) {
        return ""
    }
}

fun Date.isYesterday(): Boolean {
    return DateUtils.isToday(this.time + DateUtils.DAY_IN_MILLIS)
}

fun Date.isToday(): Boolean {
    return DateUtils.isToday(this.time)
}

fun String.getDisplayTime(): String {
    val date = Date(this.toLong())
    return when {
        date.isToday() -> "today"
        date.isYesterday() -> "yesterday"
        else -> {
            val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            format.format(date)
        }
    }
}