package com.bakery.vendorprovider.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.*
import com.bakery.vendorprovider.R

object DialogUtils {

    var loaderDialog: Dialog? = null

    fun showBottomDialog(context: Context, layout: Int): Dialog {

        val dialogView = View.inflate(context, layout, null)
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(dialogView)
        val window: Window = dialog.getWindow()!!
        window.setGravity(Gravity.BOTTOM)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        return dialog
    }

    fun showLoader(context: Context) {

        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        loaderDialog?.setContentView(view!!)
        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }

    }

    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }
}