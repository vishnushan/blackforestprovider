package com.bakery.vendorprovider.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bakery.vendorprovider.R
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun <T> Context.openActivity(it: Class<T>) {
    val intent = Intent(this, it)
    startActivity(intent)
}

fun <T> Context.openActivity(it: Class<T>, bundle: Bundle) {
    val intent = Intent(this, it)
    intent.putExtras(bundle)
    startActivity(intent)
}

fun <T> Context.openNewTaskActivity(it: Class<T>) {
    val intent = Intent(this, it)
    intent.flags =
        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(intent)
}

fun String.isValidMobileNumber(): Boolean {
    return this.length == 10
}

fun String.isValidPassword(): Boolean {
    return this.length >= 6

}

fun View.showSnack(string: String) {

    var snack = Snackbar.make(this, string, Snackbar.LENGTH_LONG)
    val view: View = snack.view
    view.setBackgroundColor(ContextCompat.getColor(this.context, R.color.green_color));
    val params = view.layoutParams as FrameLayout.LayoutParams
    params.gravity = Gravity.BOTTOM
    view.layoutParams = params
    snack.show()
}

fun Context.isNetworkConnected(): Boolean {
    val connectivityManager: ConnectivityManager? =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    var isConnected = false
    if (connectivityManager != null) {
        val activeNetwork = connectivityManager.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
    return isConnected
}

fun ImageView.loadImage(imageUrl: String?) {
    if (imageUrl == null) {
        return
    }

    Glide.with(this.context)
        .load(imageUrl)
        .apply(
            RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
        )
        .into(this)


}

fun ImageView.loadImage(imageUrl: String?, drawable: Int) {
    if (imageUrl == null) {
        return
    }

    Glide.with(this.context)
        .load(imageUrl)
        .apply(
            RequestOptions()
                .placeholder(drawable)
                .error(drawable)
        )
        .into(this)


}

fun ImageView.loadChatImage(imageUrl: String?, thumbNail: String?) {
    if (imageUrl == null) {
        return
    }

    Glide
        .with(this.context)
        .load(imageUrl)
        .apply(
            RequestOptions()
                .placeholder(R.color.white)
                .error(R.color.white)
        )
        .thumbnail(
            Glide.with(this.context)
                .load(thumbNail)
        )
        .into(this)


}

fun Context.openActivity(intent: Intent) {
    startActivity(intent)
}

fun Context.saveToGlideCache(url: String) {
    val rm: RequestManager = Glide.with(this)
    rm.load(url).submit()
}

fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
    val transaction = this.supportFragmentManager.beginTransaction()
    transaction.replace(frameId, fragment)
    transaction.commit()
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

fun View.toggleVisibility() {
    if (this.isVisible()) {
        this.visibility = View.INVISIBLE
    } else {
        this.visibility = View.VISIBLE
    }
}


fun File.isValidFileSize(): Boolean {
    println("Image file size = " + this.length())
    return this.length() <= 25000000
}

fun File.isCompresableFileSize(): Boolean {
    return this.length() >= 4000000
}


fun File.getDocumentType(): String {

    val extension = MimeTypeMap.getFileExtensionFromUrl(this.absolutePath)
    var extention = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)


    extention?.let {
        return it
    }

    return extension
}

fun File.getFileExtention(): String {

    val extension = MimeTypeMap.getFileExtensionFromUrl(this.absolutePath)
    var extention = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

    return ".$extension"
}

fun String.getFormattedDate(inputFormat: String, outputFormat: String): String {

    var spf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
    var newDate: Date? = null
    try {
        newDate = spf.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }


    spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
    return if (newDate != null)
        spf.format(newDate)
    else
        ""


}
