package com.bakery.vendorprovider.utils

import android.content.Context
import android.content.SharedPreferences


class SharedHelper(var context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)


    var firebaseToken: String
        get() : String {
            return sharedPreference.getKey("firebaseToken")
        }
        set(value) {
            sharedPreference.putKey("firebaseToken", value)
        }

    var token: String
        get() : String {
            return sharedPreference.getKey("token")
        }
        set(value) {
            sharedPreference.putKey("token", value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("loggedIn")
        }
        set(value) {
            sharedPreference.putBoolean("loggedIn", value)
        }

    var isVendor: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("isVendor")
        }
        set(value) {
            sharedPreference.putBoolean("isVendor", value)
        }


    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey("imageUploadPath")
        }
        set(value) {
            sharedPreference.putKey("imageUploadPath", value)
        }

    var userName: String
        get() : String {
            return sharedPreference.getKey("userName")
        }
        set(value) {
            sharedPreference.putKey("userName", value)
        }


     var userImage: String
        get() : String {
            return sharedPreference.getKey("userImage")
        }
        set(value) {
            sharedPreference.putKey("userImage", value)
        }

    var id: String
        get() : String {
            return sharedPreference.getKey("id")
        }
        set(value) {
            sharedPreference.putKey("id", value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey("mobileNumber")
        }
        set(value) {
            sharedPreference.putKey("mobileNumber", value)
        }

    var currentLat: String
        get() : String {
            return sharedPreference.getKey("currentLat")
        }
        set(value) {
            sharedPreference.putKey("currentLat", value)
        }

    var currentLng: String
        get() : String {
            return sharedPreference.getKey("currentLng")
        }
        set(value) {
            sharedPreference.putKey("currentLng", value)
        }

    var currentTag: String
        get() : String {
            return sharedPreference.getKey("currentTag")
        }
        set(value) {
            sharedPreference.putKey("currentTag", value)
        }

    var currentAddress: String
        get() : String {
            return sharedPreference.getKey("currentAddress")
        }
        set(value) {
            sharedPreference.putKey("currentAddress", value)
        }

    var longitude: String
        get() : String {
            return sharedPreference.getKey("longitude")
        }
        set(value) {
            sharedPreference.putKey("longitude", value)
        }

    var latitude: String
        get() : String {
            return sharedPreference.getKey("latitude")
        }
        set(value) {
            sharedPreference.putKey("latitude", value)
        }

    var orderId: String
        get() : String {
            return sharedPreference.getKey("orderId")
        }
        set(value) {
            sharedPreference.putKey("orderId", value)
        }

    var emailId: String
        get() : String {
            return sharedPreference.getKey("emailId")
        }
        set(value) {
            sharedPreference.putKey("emailId", value)
        }

    fun removeAllValues() {
        val settings: SharedPreferences =
            context.getSharedPreferences("PreferencesName", Context.MODE_PRIVATE)
        settings.edit().clear().apply()
    }

}