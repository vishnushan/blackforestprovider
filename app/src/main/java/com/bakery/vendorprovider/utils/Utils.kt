import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.TypedArray
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.TypedValue
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.bakery.vendorprovider.utils.Constants
import com.bakery.vendorprovider.utils.SharedHelper
import java.io.*
import java.nio.channels.FileChannel


fun fetchThemeColor(id: Int, context: Context): Int {
    val typedValue = TypedValue()
    val a: TypedArray =
        context.obtainStyledAttributes(typedValue.data, intArrayOf(id))
    val color = a.getColor(0, 0)
    a.recycle()
    return color
}

@SuppressLint("HardwareIds")
fun getImei(context: Context): String {
    val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    val uid: String = Settings.Secure.getString(
        context.applicationContext.contentResolver,
        Settings.Secure.ANDROID_ID
    )
    if (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.READ_PHONE_STATE
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        val imei = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                uid
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                telephonyManager.imei
            }
            else -> {
                telephonyManager.deviceId
            }
        }
        return imei
    }
    return "dosa"
}

fun isValidPassword(password: String): Boolean {
    return (password.length > 0)
}

fun isValidKryptCode(kryptCode: String): Boolean {
    return (kryptCode.length > 0)
}



fun openCamera(activity: Activity) {

    val sharedHelper = SharedHelper(activity)
    val file = getFileTostoreImage(activity)

    val uri: Uri
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        uri = FileProvider.getUriForFile(activity, activity.packageName + ".provider", file)
        sharedHelper.imageUploadPath = file.absolutePath
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)

    } else {
        uri = Uri.fromFile(file)
        sharedHelper.imageUploadPath = file.absolutePath
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)
    }
}

fun openGalleryforPhoto(activity: Activity) {
    val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    i.type = "image/*";
    activity.startActivityForResult(i, Constants.RequestCode.GALLERY_INTENT)
}
fun openGallery(activity: Activity) {
    val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    i.type = "image/* video/*";
    activity.startActivityForResult(i, Constants.RequestCode.GALLERY_INTENT)
}

fun openAudioIntent(activity: Activity) {
    val intent = Intent(Intent.ACTION_GET_CONTENT)
    intent.type = "audio/*"
    activity.startActivityForResult(intent, Constants.RequestCode.AUDIO_INTENT)
}

fun openFileIntent(activity: Activity) {

    val intent = Intent(Intent.ACTION_GET_CONTENT)
    intent.type = "*/*"
    intent.addCategory(Intent.CATEGORY_OPENABLE)
    try {
        activity.startActivityForResult(
            Intent.createChooser(intent, "Select a File to Upload"),
            Constants.RequestCode.FILE_INTENT
        );
    } catch (ex: android.content.ActivityNotFoundException) {
        Toast.makeText(
            activity, "Please install a File Manager.",
            Toast.LENGTH_SHORT
        ).show()
    }


}

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
fun getPath(context: Context, uri: Uri): String {

    return getRealPathFromUriNew(context, uri) ?: uri.toString()
}

private fun getFileTostoreImage(context: Context): File {

    val newFileName = getNewFileName(MediaType.IMAGE.value, "")

    val newFile = File(context.getExternalFilesDir(null)!!.absolutePath, "/$newFileName")


    if (!newFile.exists()) {
        newFile.createNewFile()
    }

    return newFile
}

@SuppressLint("NewApi")
fun getRealPathFromUriNew(context: Context, uri: Uri): String? {

    val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

    // DocumentProvider
    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
        // ExternalStorageProvider
        if (isExternalStorageDocument(uri)) {
            val docId = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val type = split[0]

            if ("primary".equals(type, ignoreCase = true)) {
                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            }

            //  handle non-primary volumes
        } else if (isDownloadsDocument(uri)) {

            var id = DocumentsContract.getDocumentId(uri)

            if (id != null && id.startsWith("raw:")) {
                return id.substring(4);
            }

            val contentUriPrefixesToTry = arrayOf(
                "content://downloads/public_downloads",
                "content://downloads/my_downloads",
                "content://downloads/all_downloads"
            )



            contentUriPrefixesToTry.forEachIndexed { _, s ->

                try {
                    val contentUri = ContentUris.withAppendedId(Uri.parse(s), id.toLong())
                    val path = getDataColumn(context, contentUri, null, null);
                    if (path != null) {
                        return path;
                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                }

            }


            // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
            val fileName = getFileName(context, uri)
            val cacheDir = getDocumentCacheDir(context)
            if (fileName == null) return null
            val file = generateFileName(fileName, cacheDir)
            var destinationPath: String? = null;
            if (file != null) {
                destinationPath = file.absolutePath;
                saveFileFromUri(context, uri, destinationPath);
            }

            return destinationPath
        } else if (isMediaDocument(uri)) {
            val docId = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val type = split[0]

            var contentUri: Uri? = null
            when (type) {
                "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            }
            val selection = "_id=?"
            val selectionArgs = arrayOf(split[1])

            return getDataColumn(context, contentUri, selection, selectionArgs)
        }// MediaProvider
        // DownloadsProvider
    } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

        // Return the remote address
        return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
            context,
            uri,
            null,
            null
        )

    } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
        return uri.path
    }// File
    // MediaStore (and general)

    return null
}


/**
 * Get the value of the data column for this Uri. This is useful for
 * MediaStore Uris, and other file-based ContentProviders.
 *
 * @param context       The context.
 * @param uri           The Uri to query.
 * @param selection     (Optional) Filter used in the query.
 * @param selectionArgs (Optional) Selection arguments used in the query.
 * @return The value of the _data column, which is typically a file path.
 */
private fun getDataColumn(
    context: Context, uri: Uri?, selection: String?,
    selectionArgs: Array<String>?
): String? {

    var cursor: Cursor? = null
    val column = "_data"
    val projection = arrayOf(column)

    try {
        cursor =
            context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
        if (cursor != null && cursor.moveToFirst()) {
            val index = cursor.getColumnIndexOrThrow(column)
            return cursor.getString(index)
        }
    } finally {
        cursor?.close()
    }
    return null
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is ExternalStorageProvider.
 */
private fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.authority
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is DownloadsProvider.
 */
private fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.authority
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is MediaProvider.
 */
private fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.authority
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is Google Photos.
 */
private fun isGooglePhotosUri(uri: Uri): Boolean {
    return "com.google.android.apps.photos.content" == uri.authority
}

fun getRealPathFromURI(context: Context, contentURI: Uri): String? {
    val result: String?
    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
    val cursor = context.contentResolver.query(contentURI, filePathColumn, null, null, null)

    if (cursor == null) {
        result = contentURI.path
    } else {
        cursor.moveToFirst()
        val idx = cursor
            .getColumnIndex(filePathColumn[0])
        result = cursor.getString(idx)
        cursor.close()
    }
    return result
}

fun getDocumentCacheDir(context: Context): File {
    var dir = File(context.cacheDir, "documents")
    if (!dir.exists()) {
        dir.mkdirs();
    }

    return dir;
}


@RequiresApi(api = Build.VERSION_CODES.KITKAT)
fun getFileName(context: Context, uri: Uri): String? {
    var mimeType = context.getContentResolver().getType(uri);
    var filename: String? = null

    if (mimeType == null && context != null) {
        var path = getPath(context, uri);
        if (path == null) {
            filename = getName(uri.toString());
        } else {
            var file = File(path);
            filename = file.getName();
        }
    } else {
        var returnCursor = context.getContentResolver().query(
            uri, null,
            null, null, null
        );
        if (returnCursor != null) {
            var nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            returnCursor.moveToFirst();
            filename = returnCursor.getString(nameIndex);
            returnCursor.close();
        }
    }

    return filename
}


fun getName(filename: String): String? {
    if (filename == null) {
        return null
    }
    var index = filename.lastIndexOf('/');
    return filename.substring(index + 1);
}


@Nullable
fun generateFileName(namePar: String, directory: File): File? {

    var name: String = namePar

    var file = File(directory, name)

    if (file.exists()) {
        var fileName = name;
        var extension = "";
        var dotIndex = name.lastIndexOf('.');
        if (dotIndex > 0) {
            fileName = name.substring(0, dotIndex);
            extension = name.substring(dotIndex);
        }

        var index = 0;
        file.delete();

        while (file.exists()) {
            index++;
            name = fileName + extension;
            file = File(directory, name)
        }
    }

    try {
        if (!file.createNewFile()) {
            return null;
        }
    } catch (e: IOException) {

        return null;
    }


    return file;
}

private fun saveFileFromUri(context: Context, uri: Uri, destinationPath: String) {
    var iss: InputStream? = null;
    var bos: BufferedOutputStream? = null
    try {
        iss = context.getContentResolver().openInputStream(uri);
        bos = BufferedOutputStream(FileOutputStream(destinationPath, false));
        var buf = ByteArray(1024)
        iss?.read(buf);
        do {
            bos.write(buf);
        } while (iss?.read(buf) != -1);
    } catch (e: IOException) {
        e.printStackTrace();
    } finally {
        try {
            iss?.close()
            bos?.close()
        } catch (e: IOException) {
            e.printStackTrace();
        }
    }
}

fun getNewFileName(fileType: Int, docName: String): String {

    return when (fileType) {
        MediaType.IMAGE.value -> ".IMG_" + "_" + System.currentTimeMillis().toString() + ".jpg"
        MediaType.VIDEO.value -> ".VID_" + "_" + System.currentTimeMillis().toString() + ".mp4"
        MediaType.DOCUMENT.value -> ".$docName"
        MediaType.AUDIO.value -> ".AUD_" + "_" + System.currentTimeMillis().toString() + ".mp3"
        else -> ".IMG_" + "_" + System.currentTimeMillis().toString() + ".jpg"
    }
}

fun getViewIntent(uri: Uri): Intent {
    //Uri uri = Uri.parse(uripath);

    var intent = Intent(Intent.ACTION_VIEW)
    var url = uri.toString();
    if (url.contains(".doc") || url.contains(".docx")) {
        // Word document
        intent.setDataAndType(uri, "application/msword");
        // intent.setData(uri);
    } else if (url.contains(".pdf")) {
        // PDF file
        intent.setDataAndType(uri, "application/pdf");
        //intent.setData(uri);

    } else if (url.contains(".ppt") || url.contains(".pptx")) {
        // Powerpoint file
        intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
    } else if (url.contains(".xls") || url.contains(".xlsx")) {
        // Excel file
        intent.setDataAndType(uri, "application/vnd.ms-excel");
    } else if (url.contains(".zip") || url.contains(".rar")) {
        // WAV audio file
        intent.setDataAndType(uri, "application/x-wav");
    } else if (url.contains(".rtf")) {
        // RTF file
        intent.setDataAndType(uri, "application/rtf");
    } else if (url.contains(".wav") || url.contains(".mp3")) {
        // WAV audio file
        intent.setDataAndType(uri, "audio/x-wav");
    } else if (url.contains(".gif")) {
        // GIF file
        intent.setDataAndType(uri, "image/gif");
    } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
        // JPG file
        intent.setDataAndType(uri, "image/jpeg");
    } else if (url.contains(".txt")) {
        // Text file
        intent.setDataAndType(uri, "text/plain");
    } else if (url.contains(".3gp") || url.contains(".mpg") || url.contains(".mpeg") ||
        url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi") ||
        url.contains(".webm") || url.contains(".m4v") || url.contains(".mkv")
    ) {
        // Video files
        intent.setDataAndType(uri, "video/*");
    } else {
        intent.setDataAndType(uri, "*/*");
    }

    //   intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    //  intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    //   intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    return intent
}

@Throws(IOException::class)
fun copyFile(context: Context, src: File): File {

    val filename = "." + src.name

    val newFile = File(context.getExternalFilesDir(null)!!.absolutePath, "/$filename")

    if (!newFile.exists()) {
        newFile.createNewFile()
    }


    val inStream = FileInputStream(src)
    val outStream = FileOutputStream(newFile)
    val inChannel: FileChannel = inStream.channel
    val outChannel: FileChannel = outStream.channel
    inChannel.transferTo(0, inChannel.size(), outChannel)
    inStream.close()
    outStream.close()

    return newFile
}

enum class MediaType(val value: Int) {
    NONE(0),
    IMAGE(1),
    VIDEO(2),
    DOCUMENT(3),
    AUDIO(4)
}
