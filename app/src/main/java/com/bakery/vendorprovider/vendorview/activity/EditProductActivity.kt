package com.bakery.vendorprovider.vendorview.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.loadImage
import com.bakery.vendorprovider.utils.showSnack
import com.bakery.vendorprovider.view.activity.BaseActivity
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.activity_product_details.*

class EditProductActivity : BaseActivity() {

    var commonViewModel: CommonViewModel? = null
    var productId = 0
    var status1 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)

        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        getIntentValues()

        initListener()
    }

    private fun initListener() {

        update.setOnClickListener {

            if (productId != 0) {
                validInputs().let {
                    if (it == "") {
                        commonViewModel?.updateProductDetails(
                            this,
                            mobileNumber.text.toString().trim(),
                            quandity.text.toString().trim(),
                            price.text.toString().trim(),
                            description.text.toString().trim(),
                            productId,
                            status1
                        )?.observe(this, Observer {
                            if (it.error) {
                                rootLayout.showSnack(it.msg)
                            } else {
                                finish()
                            }
                        })
                    } else {
                        rootLayout.showSnack(it)
                    }
                }
            }

        }
    }

    private fun validInputs(): String {

        return if (mobileNumber.text.toString().trim().isEmpty()) {
            "Enter product name"
        } else if (quandity.text.toString().trim().isEmpty()) {
            "Enter product quantity"
        } else if (price.text.toString().trim().isEmpty()) {
            "Enter product price"
        } else if (description.text.toString().trim().isEmpty()) {
            "Enter product Description"
        } else {
            ""
        }

    }

    private fun getIntentValues() {

        intent.extras?.let {
            productId = it.getInt("id")
            commonViewModel?.getProductDetails(this, productId)?.observe(this, Observer { detail ->
                if (!detail.error) {
                    if (detail.productList?.size != 0) {
                        detail.productList?.get(0)?.let { productDetails ->

                            imageUrl.loadImage(productDetails.ImageUrl, R.mipmap.ic_launcher)
                            mobileNumber.setText(productDetails.name)
                            quandity.setText(productDetails.quantity.toString())
                            price.setText(productDetails.price)
                            description.setText(productDetails.description)

                            status1 = productDetails.status
                            setui()

                        }

                    }
                }
            })
        }


        status.setOnClickListener {
            status1 = if (status1 == 0) {
                1
            } else {
                0
            }

            setui()
        }
    }

    private fun setui() {

        if (status1 == 0) {

            status.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.switch_off
                )
            )
            status.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.heart_colour
                )
            )

        } else {

           status.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.switch_on
                )
            )
            status.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.green_color
                )
            )

        }

    }
}