package com.bakery.vendorprovider.vendorview.activity

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.view.activity.BaseActivity
import kotlinx.android.synthetic.main.activity_main_vendor.*

class VendorMainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_vendor)

        bottomNavigation.setupWithNavController(nav_host.findNavController())

    }
}