package com.bakery.vendorprovider.vendorview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.databinding.ChildOrderVendorBinding
import com.bakery.vendorprovider.models.OrderListVendorData
import com.bakery.vendorprovider.utils.hide
import com.bakery.vendorprovider.utils.show

class OrderListAdapter(
    var context: Context,
    var list: ArrayList<OrderListVendorData>?,
    var orderAccept: (String?) -> Unit,
    var orderreject: (String?) -> Unit
) :
    RecyclerView.Adapter<OrderListAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var binding: ChildOrderVendorBinding = ChildOrderVendorBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_order_vendor, parent, false)
        )
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.binding.status.text = list?.get(position)?.order_status
        holder.binding.name.text = list?.get(position)?.productName
        holder.binding.referenceId.text = "ORDER NUMBER : " + list?.get(position)?.refference_Id

        holder.binding.address.text = list?.get(position)?.address

        holder.binding.amount.text = "₹ " + list?.get(position)?.total

        if (list?.get(position)?.order_status.equals("progress", true)) {
            holder.binding.acceptRejectLayout.show()
        } else {
            holder.binding.acceptRejectLayout.hide()
        }

        holder.binding.acceptOrder.setOnClickListener {
            orderAccept(list?.get(position)?.id)
        }

        holder.binding.rejectOrder.setOnClickListener {
            orderreject(list?.get(position)?.id)
        }


    }

    fun notifydata(list: ArrayList<OrderListVendorData>?) {
        this.list = list
        notifyDataSetChanged()
    }


}