package com.bakery.vendorprovider.vendorview.fragment

import android.os.Bundle
import android.os.SharedMemory
import android.view.View
import androidx.fragment.app.Fragment
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.utils.loadImage
import com.bakery.vendorprovider.utils.openActivity
import com.bakery.vendorprovider.view.activity.BaseActivity
import com.bakery.vendorprovider.view.activity.SelectLoginType
import kotlinx.android.synthetic.main.fragment_account_vendor.*

class AccountFragment : Fragment(R.layout.fragment_account_vendor) {

    var sharedHelper : SharedHelper? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedHelper = SharedHelper(requireContext())

        name.text = sharedHelper?.userName
        email.text = sharedHelper?.emailId

        profileImage.loadImage(sharedHelper?.userName,R.drawable.profile_tab_icon_grey)

        logout.setOnClickListener {
            sharedHelper?.removeAllValues()
            requireContext().openActivity(SelectLoginType::class.java)

        }
    }
}