package com.bakery.vendorprovider.vendorview.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.vendorview.adapter.OrderListAdapter
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.fragment_home_vendor.*

class HomeFragment : Fragment(R.layout.fragment_home_vendor) {

    private val commonViewModel: CommonViewModel by viewModels()
    var sharedHelper: SharedHelper? = null
    var orderListAdapter: OrderListAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedHelper = SharedHelper(requireContext())
        updateToken()
        getDetails()
        setAdapter()
        initListener()
    }

    private fun setAdapter() {

        orderListAdapter = OrderListAdapter(requireContext(), ArrayList(), {
            commonViewModel.updateOrderAccepted(requireContext(), it, "accepted")
                .observe(viewLifecycleOwner,
                    Observer {
                        if (!it.error) {
                            getOrderDetails()
                        }
                    })
        }, {
            commonViewModel.updateOrderRejected(requireContext(), it, "rejected")
                .observe(viewLifecycleOwner,
                    Observer {
                        if (!it.error) {
                            getOrderDetails()
                        }
                    })
        })

        orderList.layoutManager = LinearLayoutManager(requireContext())
        orderList.adapter = orderListAdapter
    }

    override fun onResume() {
        super.onResume()
        getOrderDetails()
    }


    private fun initListener() {

        rdt_on.setOnCheckedChangeListener { _, _ ->
            if (rdt_on.isChecked) {
                commonViewModel.updateVendorStatus(requireContext(), 1)
                    .observe(viewLifecycleOwner, Observer {
                        getDetails()
                    })
            }
        }

        rdt_off.setOnCheckedChangeListener { _, _ ->
            if (rdt_off.isChecked) {
                commonViewModel.updateVendorStatus(requireContext(), 0)
                    .observe(viewLifecycleOwner, Observer {
                        getDetails()
                    })
            }
        }

    }

    private fun getDetails() {

        commonViewModel.getDetails(requireContext()).observe(viewLifecycleOwner, Observer {
            if (!it.error) {
                name.text = it?.providerDetails?.name
                address.text = it?.providerDetails?.addressLineOne

                if (it.providerDetails?.status == "1") {
                    rdt_on.isChecked = true
                } else {
                    rdt_off.isChecked = true
                }

                amountText.text = it?.yarnings?.todayEarning
                amountText2.text = it?.yarnings?.weekEarnings
                amountText3.text = it?.yarnings?.yesterdayEarnings


                order.text = it?.yarnings?.todayOrders + " Orders"
                order2.text = it?.yarnings?.weekOrderd + " Orders"
                order3.text = it?.yarnings?.yesterdayOrders + " Orders"

            }
        })
    }


    private fun updateToken() {

        commonViewModel.updateDeviceToken(requireContext())
    }


    private fun getOrderDetails() {

        commonViewModel.getOrders(requireContext()).observe(viewLifecycleOwner) { response ->

            if (!response.error) {
                orderListAdapter?.notifydata(response.orderlist)
            }
        }

    }
}