package com.bakery.vendorprovider.vendorview.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.DialogUtils
import com.bakery.vendorprovider.view.adapter.ProductListAdapter
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.fragment_product.*

class ProductFragment : Fragment(R.layout.fragment_product) {

    var commonViewModel: CommonViewModel? = null
    var adapter: ProductListAdapter? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)

        initAdapter()
    }

    private fun initAdapter() {

        productList.layoutManager = LinearLayoutManager(requireContext())


        adapter = ProductListAdapter(requireContext(), ArrayList()) { position, status, id ->

            DialogUtils.showLoader(requireContext())
            commonViewModel?.updateProductStatus(
                requireContext(),
                id,
                status
            )
                ?.observe(viewLifecycleOwner,
                    Observer {

                        DialogUtils.dismissLoader()
                        if (!it.error) {
                            adapter?.notifydataChanged(position)
                        }
                    })
        }
        productList.adapter = adapter

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {

        commonViewModel?.getProductList(requireContext())?.observe(viewLifecycleOwner, Observer {

            if (!it.error) {
                adapter?.notifyList(it.productList)
            }
        })

    }
}