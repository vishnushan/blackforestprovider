package com.bakery.vendorprovider.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.Constants
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.utils.isValidMobileNumber
import com.bakery.vendorprovider.utils.showSnack
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity() {


    private var userNameLayout: ConstraintSet = ConstraintSet()
    private var passwordNameLayout: ConstraintSet = ConstraintSet()
    private var viewModel: CommonViewModel? = null
    private var sharedHelper = SharedHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        userNameLayout.clone(this, R.layout.activity_login)
        userNameLayout.connect(
            R.id.signingView,
            ConstraintSet.BOTTOM,
            R.id.root,
            ConstraintSet.BOTTOM
        )

        userNameLayout.connect(
            R.id.signingView,
            ConstraintSet.TOP,
            R.id.root,
            ConstraintSet.TOP
        )

        userNameLayout.setVerticalBias(R.id.signingView, 1f)
        passwordNameLayout.clone(userNameLayout)
        passwordNameLayout.setVisibility(R.id.password, View.VISIBLE)
        passwordNameLayout.setVisibility(R.id.forgotPasswrod, View.VISIBLE)


        Handler().postDelayed({
            showAnimated()
        }, 250)

        initListener()


    }

    private fun initListener() {
        login.setOnClickListener {

//            if (password.visibility == View.VISIBLE) {
            loginUser()
//            } else {
//                checkMobileExist()
//            }
        }

//        forgotPasswrod.setOnClickListener {
//            if (mobileNumber.text.toString().isValidMobileNumber()) {
//                startActivity(
//                    Intent(this, PhoneNumberVerificationActivity::class.java)
//                        .putExtra(
//                            Constants.IntentKeys.MOBILE_NUMNER,
//                            mobileNumber.text.toString()
//                        )
//                        .putExtra(
//                            Constants.IntentKeys.FORGOTPASSWORDFLOW,
//                            true
//                        )
//                )
//            } else {
//                root.showSnack(getString(R.string.enter_your_registered_mobile_number))
//            }
//        }
    }

    private fun loginUser() {

//        if (mobileNumber.text.toString().isValidMobileNumber()
//        ) {
            viewModel?.getOtp(this, mobileNumber.text.toString())
                ?.observe(this,
                    Observer {
                        if (it.error) {
                            root.showSnack(it.msg)
                        } else {


                            startActivity(
                                Intent(this, PhoneNumberVerificationActivity::class.java)
                                    .putExtra(
                                        Constants.IntentKeys.MOBILE_NUMNER,
                                        mobileNumber.text.toString()
                                    )
                            )
                        }
                    })
//        } else {
//            root.showSnack(getString(R.string.valid_credentials))
//        }
    }


    fun showAnimated() {
        val transition = ChangeBounds()
        transition.interpolator = LinearInterpolator()
        transition.duration = 400

        TransitionManager.beginDelayedTransition(root, transition)
        userNameLayout.applyTo(root)
    }

    fun showPassword() {
        val transition = ChangeBounds()
        transition.interpolator = LinearInterpolator()
        transition.duration = 400

        TransitionManager.beginDelayedTransition(root, transition)
        passwordNameLayout.applyTo(root)
    }

    fun hidePassword() {
        val transition = ChangeBounds()
        transition.interpolator = LinearInterpolator()
        transition.duration = 400

        TransitionManager.beginDelayedTransition(root, transition)
        userNameLayout.applyTo(root)
    }

    override fun onBackPressed() {
        if (password.visibility == View.VISIBLE) {
            hidePassword()
        } else {
            super.onBackPressed()
        }
    }

}