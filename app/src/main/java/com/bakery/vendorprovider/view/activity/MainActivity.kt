package com.bakery.vendorprovider.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.UiUtils.BottomNavigationViewHelper
import com.bakery.vendorprovider.view.adapter.HomeViewPagerAdapter
import com.bakery.vendorprovider.view.fragment.AccountFragment
import com.bakery.vendorprovider.view.fragment.OrdersFragment
import com.bakery.vendorprovider.view.fragment.RideFragment
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var commonViewModel : CommonViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        supportActionBar?.hide()
        init()

    }


    private fun init() {
        BottomNavigationViewHelper.removeShiftMode(bottomNavigation)
        val adapter = HomeViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(RideFragment(), "")
        adapter.addFragment(OrdersFragment(), "")
        adapter.addFragment(AccountFragment(), "")
        viewPager.adapter = adapter

        bottomNavigation.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.ride_item -> {
                    viewPager.currentItem = 0
                    return@OnNavigationItemSelectedListener true
                }
                R.id.orders_item -> {
                    viewPager.currentItem = 1
                    return@OnNavigationItemSelectedListener true
                }
                R.id.account_item -> {
                    viewPager.currentItem = 2
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })

    }

    override fun onResume() {
        super.onResume()
        commonViewModel?.updateDeviceTokenDelivery(this)
    }
}