package com.bakery.vendorprovider.view.activity

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.background.LocationEmitter
import com.bakery.vendorprovider.models.OrderDetailsData
import com.bakery.vendorprovider.rxbus.RxBusNotification
import com.bakery.vendorprovider.utils.Constants
import com.bakery.vendorprovider.utils.hide
import com.bakery.vendorprovider.utils.show
import com.bakery.vendorprovider.view.adapter.ItemsAdapter
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_order_detail.*
import kotlinx.android.synthetic.main.order_detail_content.*

class OrderDetailsActivity : BaseActivity() {

    lateinit var commonViewModel: CommonViewModel
    var orderId = ""
    private lateinit var disposable: Disposable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)
        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        intent.extras?.let {
            it.getString("orderId")?.let {
                orderId = it
                getOrderDetails()
            }
        }

        disposable = RxBusNotification.listen(String::class.java).doOnError {
        }.subscribe {
            runOnUiThread {
                getOrderDetails()
            }
        }

        getOrderDetails()
    }

    private fun getOrderDetails() {

        contentLayout.hide()
        loadingLayout.show()
        commonViewModel.getOrderDetails(this, orderId).observe(this, Observer {

            contentLayout.show()
            loadingLayout.hide()

            if (!it.error) {
                it.orderDetails?.let { it1 -> setDetails(it1) }
            }
        })

    }

    private fun setDetails(orderDetails: OrderDetailsData) {

        customerNameToolbar.text = orderDetails.userName
        restaurantName.text = orderDetails.storeName
        restaurantAddress.text = orderDetails.storeAddress
        customerName.text = orderDetails.userName
        customerAddress.text = orderDetails.userAddress
        orderStatus.text = orderDetails.orderStatus
        buttonText.text = orderDetails.orderStatus
        orderNumber.text = orderDetails.orderRefferenceId
        itemAndCost.text =
            orderDetails.orderProductDetails?.size.toString() + "items * " + orderDetails.amount

        when (orderDetails.orderStatus) {
            Constants.OrderStatus.ASSIGNED -> {
                startButton.hide()
            }
            Constants.OrderStatus.ACCEPTED -> {
                startButton.show()
                buttonText.text = "Picked up"
            }
            Constants.OrderStatus.PICKEDUP -> {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(Intent(this, LocationEmitter::class.java))
                } else {
                    startService(Intent(this, LocationEmitter::class.java))
                }

                startButton.show()
                buttonText.text = "Delivered"
            }
            else -> {
                startButton.hide()
            }
        }



        customerNavigate.setOnClickListener {

            var location =
                "http://maps.google.com/maps?saddr=${orderDetails.storeLat},${orderDetails.storeLng}&daddr=${orderDetails.userLat},${orderDetails.userLng}"
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(location)
            )
            startActivity(intent)
        }

        startButton.setOnClickListener {
            when (orderDetails.orderStatus) {

                Constants.OrderStatus.ACCEPTED -> {
                    commonViewModel.updateStatus(
                        this,
                        orderDetails.orderId.toString(),
                        Constants.OrderStatus.PICKEDUP
                    ).observe(this, Observer {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(Intent(this, LocationEmitter::class.java))
                        } else {
                            startService(Intent(this, LocationEmitter::class.java))
                        }

                        finish()
                    })
                }
                Constants.OrderStatus.PICKEDUP -> {
                    commonViewModel.updateStatus(
                        this,
                        orderDetails.orderId.toString(),
                        Constants.OrderStatus.DELIVERED
                    ).observe(this, Observer {
                        stopService(Intent(this, LocationEmitter::class.java))
                        finish()
                    })
                }

            }
        }


        dishesRecycler.layoutManager = LinearLayoutManager(this)
        dishesRecycler.adapter = ItemsAdapter(this, orderDetails.orderProductDetails)

    }


}