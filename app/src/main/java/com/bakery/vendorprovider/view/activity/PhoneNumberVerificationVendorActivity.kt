package com.bakery.vendorprovider.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.models.VendorDetails
import com.bakery.vendorprovider.utils.Constants
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.utils.showSnack
import com.bakery.vendorprovider.vendorview.activity.VendorMainActivity
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_otp_verification.*
import java.util.concurrent.TimeUnit


class PhoneNumberVerificationVendorActivity : BaseActivity() {


    var mobileNumberValue = ""
    private var viewModel: CommonViewModel? = null
    private var isForgotPasswordFlow = false

    var storedVerificationId = ""
    var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    lateinit var auth: FirebaseAuth
    lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        viewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        auth = FirebaseAuth.getInstance()
        getIntentValues()
        initListener()

    }

    @SuppressLint("SetTextI18n")
    private fun getIntentValues() {

        intent.extras?.let {
            it.getString(Constants.IntentKeys.MOBILE_NUMNER)?.let { number ->
                mobileNumber.text = "+91 $number"
                mobileNumberValue = number
                getSms()
            }

            it.getBoolean(Constants.IntentKeys.FORGOTPASSWORDFLOW).let { isFlow ->
                isForgotPasswordFlow = isFlow
            }
        }
    }

    private fun initListener() {
        verify.setOnClickListener {


            if (otp1.text.toString() != "" && otp2.text.toString() != "" && otp3.text.toString() != ""
                && otp4.text.toString() != ""
                && otp5.text.toString() != ""
                && otp6.text.toString() != ""
            ) {
                verifyPhoneNumber("${otp1.text}${otp2.text}${otp3.text}${otp4.text}${otp5.text}${otp6.text}")
            } else {
                root.showSnack(getString(R.string.valid_otp))
            }

        }


        resendOtp.setOnClickListener {

            getOtp()
            resendOtp.isEnabled = false

            Handler().postDelayed({
                resendOtp.isEnabled = true
            }, 10000)
        }

        otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.isNotEmpty()) {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })

        otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.isNotEmpty()) {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                } else {
                    otp1.isFocusable = true
                    otp1.requestFocus()
                }

            }
        })

        otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.isNotEmpty()) {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })

        otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp5.setFocusable(true)
                    otp5.requestFocus()
                } else {
                    otp3.setFocusable(true)
                    otp3.requestFocus()
                }
            }
        })


        otp5.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp6.setFocusable(true)
                    otp6.requestFocus()
                } else {
                    otp4.setFocusable(true)
                    otp4.requestFocus()
                }
            }
        })


        otp6.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp6.setFocusable(true)
                    otp6.requestFocus()
                }
            }
        })


        otp2.setOnKeyListener(View.OnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp2.getText().toString().isEmpty()) {
                    otp1.setText("")
                    otp1.requestFocus()
                }
            }
            false
        })


        otp3.setOnKeyListener(View.OnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp3.getText().toString().isEmpty()) {
                    otp2.setText("")
                    otp2.requestFocus()
                }
            }
            false
        })

        otp4.setOnKeyListener(View.OnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp4.getText().toString().isEmpty()) {
                    otp3.setText("")
                    otp3.requestFocus()
                }
            }
            false
        })

        otp5.setOnKeyListener(View.OnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp5.getText().toString().isEmpty()) {
                    otp4.setText("")
                    otp4.requestFocus()
                }
            }
            false
        })

        otp6.setOnKeyListener(View.OnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp6.getText().toString().isEmpty()) {
                    otp5.setText("")
                    otp5.requestFocus()
                }
            }
            false
        })
    }

    private fun moveToNext(
        providerDetails: VendorDetails?) {

        var sharedHelper = SharedHelper(this)
//        sharedHelper.token = accesstoken
        sharedHelper.id = providerDetails?.id.toString()
        sharedHelper.userName =
            providerDetails?.name.toString()
        sharedHelper.isVendor = true
        sharedHelper.userImage = providerDetails?.image.toString()
        sharedHelper.currentAddress = providerDetails?.addressLineOne.toString()

        sharedHelper.loggedIn = true
        sharedHelper.emailId = providerDetails?.email.toString()


        startActivity(
            Intent(this, VendorMainActivity::class.java)
                .putExtra(Constants.IntentKeys.MOBILE_NUMNER, mobileNumberValue)
        )

    }

    fun getOtp() {
        viewModel?.getOtp(this, mobileNumberValue)?.observe(this, Observer {
            root.showSnack(it.msg)
        })
    }

    fun getSms() {

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {

                otpVerified()
            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.d("Error ", "onVerificationFailed", e)

                if (e is FirebaseAuthInvalidCredentialsException) {
                    root.showSnack("Invalid Credentials")
                } else if (e is FirebaseTooManyRequestsException) {
                    root.showSnack("Please Try Later")
                }

                // Show a message and update the UI
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {

                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token
            }
        }

        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber("+91$mobileNumberValue")       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    fun verifyPhoneNumber(s: String) {
        val credential = PhoneAuthProvider.getCredential(storedVerificationId, s)


        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = task.result?.user

                    otpVerified()
                } else {
                    // Sign in failed, display a message and update the UI
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        root.showSnack("Invalid Otp")
                    }

                }
            }


    }

    private fun otpVerified(){

        viewModel?.verifyOtpVendor(
            this,
            "1234",
            mobileNumberValue
        )?.observe(this,
            Observer {
                if (it.error) {
                    root.showSnack(it.msg)
                } else {
                    moveToNext(it.providerDetails)
                }
            })

    }
}