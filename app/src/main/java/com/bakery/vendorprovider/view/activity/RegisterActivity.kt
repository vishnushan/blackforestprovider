package com.bakery.vendorprovider.view.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.Constants
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity() {

    private var viewModel: CommonViewModel? = null
    private var sharedHelper = SharedHelper(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        viewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        initListener()
        getIntentValues()
    }

    @SuppressLint("SetTextI18n")
    private fun getIntentValues() {

        intent.extras?.let {
            it.getString(Constants.IntentKeys.MOBILE_NUMNER)?.let { number ->
                mobileNumber.setText("$number")
            }
        }

    }

    private fun initListener() {
        register.setOnClickListener {

//            if (name.text.toString() != "" && password.text.toString().isValidPassword()) {
//                viewModel?.registerUser(
//                    this,
//                    name.text.toString(),
//                    mobileNumber.text.toString(),
//                    password.text.toString()
//                )?.observe(this,
//                    Observer {
//                        if (it.error) {
//                            root.showSnack(it.msg)
//                        } else {
//                            sharedHelper.loggedIn = true
//                            it.userDetails?.id?.let { sharedHelper.id = it }
//                            it.userDetails?.userName?.let { sharedHelper.userName = it }
//                            it.userDetails?.mobileNumber?.let { sharedHelper.mobileNumber = it }
//                            it.accesstoken?.let { sharedHelper.token = it }
//                            openNewTaskActivity(DashboardActivity::class.java)
//                        }
//                    })
//            } else {
//                root.showSnack(getString(R.string.enter_details))
//            }
        }

    }

}
