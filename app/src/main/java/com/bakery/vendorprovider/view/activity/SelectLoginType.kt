package com.bakery.vendorprovider.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.openNewTaskActivity
import kotlinx.android.synthetic.main.activity_select_login_type.*

class SelectLoginType : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_login_type)


        initListener()
    }

    private fun initListener() {

        driver_image.setOnClickListener {
            openNewTaskActivity(LoginActivity::class.java)
        }

        vendor_image.setOnClickListener {
            openNewTaskActivity(LoginVendorActivity::class.java)
        }

    }
}