package com.bakery.vendorprovider.view.activity

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.utils.isNetworkConnected
import com.bakery.vendorprovider.utils.openNewTaskActivity
import com.bakery.vendorprovider.vendorview.activity.VendorMainActivity

class SplashScreenActivity : AppCompatActivity() {

    var handler = Handler()
    lateinit var runnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        runnable = Runnable {
            if (isNetworkConnected()) {
                if (SharedHelper(this).loggedIn) {
                    if(SharedHelper(this).isVendor){
                        openNewTaskActivity(VendorMainActivity::class.java)
                    }else{
                        openNewTaskActivity(MainActivity::class.java)
                    }

                } else {
                    openNewTaskActivity(SelectLoginType::class.java)
                }
            }

        }
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(runnable, 2000)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

}