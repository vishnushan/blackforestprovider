package com.bakery.vendorprovider.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.databinding.ChildItemsBinding
import com.bakery.vendorprovider.models.OrderedProductDetails

class ItemsAdapter(var context: Context, var list: ArrayList<OrderedProductDetails>?) :
    RecyclerView.Adapter<ItemsAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var binding: ChildItemsBinding = ChildItemsBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_items, parent, false)
        )
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.binding.dishName.text = list?.get(position)?.productName
        holder.binding.displayPrice.text = list?.get(position)?.price
    }
}