package com.bakery.vendorprovider.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.databinding.ChildOrderListBinding
import com.bakery.vendorprovider.models.OrderListData
import com.bakery.vendorprovider.view.activity.OrderDetailsActivity

class OrderListAdapter(var context: Context, var list: ArrayList<OrderListData>?) :
    RecyclerView.Adapter<OrderListAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var binding: ChildOrderListBinding = ChildOrderListBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_order_list, parent, false)
        )
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.binding.orderID.text = list?.get(position)?.refference_Id
        holder.binding.status.text = list?.get(position)?.order_status
        holder.binding.userName.text = list?.get(position)?.userName
        holder.binding.address.text = list?.get(position)?.address
        holder.binding.amount.text = list?.get(position)?.total

        holder.itemView.setOnClickListener {
            var intent = Intent(context, OrderDetailsActivity::class.java)
            intent.putExtra("orderId", list?.get(position)?.id.toString())
            context.startActivity(intent)
        }

    }
}