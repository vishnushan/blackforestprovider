package com.bakery.vendorprovider.view.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.databinding.ChildProductListBinding
import com.bakery.vendorprovider.models.ProductList
import com.bakery.vendorprovider.utils.loadImage
import com.bakery.vendorprovider.utils.openActivity
import com.bakery.vendorprovider.vendorview.activity.EditProductActivity

class ProductListAdapter(
    var context: Context,
    var list: ArrayList<ProductList>?,
    var selectedItem: (Int, Int, Int) -> Unit
) :
    RecyclerView.Adapter<ProductListAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var binding: ChildProductListBinding = ChildProductListBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_product_list, parent, false)
        )
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.binding.dishName.text = list?.get(position)?.name
        holder.binding.imageUrl.loadImage(list?.get(position)?.ImageUrl, R.mipmap.ic_launcher)

        if (list?.get(position)?.status == 0) {

            holder.binding.status.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.switch_off
                )
            )
            holder.binding.status.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.heart_colour
                )
            )

        } else {

            holder.binding.status.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.switch_on
                )
            )
            holder.binding.status.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.green_color
                )
            )

        }

        holder.binding.cardView2.setOnClickListener {

            val bundle = Bundle()
            bundle.putInt("id", list?.get(position)?.productId!!)
            context.openActivity(EditProductActivity::class.java, bundle)

        }
        holder.binding.dishName.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("id", list?.get(position)?.productId!!)
            context.openActivity(EditProductActivity::class.java, bundle)
        }



        holder.binding.status.setOnClickListener {

            val status = if (list?.get(position)?.status == 0) {
                1
            } else {
                0
            }
            selectedItem(position, status, list?.get(position)?.productId!!)
        }

    }

    fun notifydataChanged(position: Int) {
        val status = if (list?.get(position)?.status == 0) {
            1
        } else {
            0
        }
        list?.get(position)?.status = status
        notifyItemChanged(position)
    }

    fun notifyList(productList: java.util.ArrayList<ProductList>) {
        list = productList
        notifyDataSetChanged()
    }
}