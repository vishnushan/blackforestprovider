package com.bakery.vendorprovider.view.fragment

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.DialogUtils.showBottomDialog
import com.bakery.vendorprovider.utils.SharedHelper
import com.bakery.vendorprovider.utils.openNewTaskActivity
import com.bakery.vendorprovider.view.activity.SelectLoginType
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.fragment_account.*


class AccountFragment : Fragment(R.layout.fragment_account) {

    lateinit var sharedHelper: SharedHelper
    lateinit var userName: EditText
    lateinit var emailId: EditText
    lateinit var commonViewModel: CommonViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedHelper = SharedHelper(requireContext())
        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        initListener()
        setPreferenceValue()
    }

    private fun initListener() {

        logout.setOnClickListener {
            sharedHelper.loggedIn = false
            sharedHelper.removeAllValues()
            requireContext().openNewTaskActivity(SelectLoginType::class.java)
        }

        edit.setOnClickListener {
            showEditDialog()
        }
    }

    private fun setPreferenceValue() {

        name.text = sharedHelper.userName
        email.text = sharedHelper.emailId
        phoneNumber.text = "+91 " + sharedHelper.mobileNumber


    }


    private fun showEditDialog() {

        val editDialog = showBottomDialog(requireContext(), R.layout.dialog_editname)
        userName = editDialog.findViewById(R.id.userName)
        emailId = editDialog.findViewById(R.id.email)

        userName.setText(sharedHelper.userName)
        emailId.setText(sharedHelper.emailId)

        editDialog.findViewById<TextView>(R.id.update).setOnClickListener {
            if (userName.text.trim().toString().isNotEmpty()
                && emailId.text.trim().toString().isNotEmpty()
                && Patterns.EMAIL_ADDRESS.matcher(emailId.text.trim().toString()).matches()
            )
                commonViewModel.updateProfile(
                    requireContext(),
                    userName.text.trim().toString(),
                    emailId.text.trim().toString()
                ).observe(viewLifecycleOwner, Observer {
                    if (!it.error) {
                        sharedHelper.emailId = emailId.text.trim().toString()
                        sharedHelper.userName = userName.text.trim().toString()
                        setPreferenceValue()
                    }
                    editDialog.dismiss()
                })
        }

        editDialog.show()
    }


}