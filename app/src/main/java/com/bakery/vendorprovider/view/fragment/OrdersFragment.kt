package com.bakery.vendorprovider.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.utils.hide
import com.bakery.vendorprovider.utils.show
import com.bakery.vendorprovider.view.adapter.OrderListAdapter
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import kotlinx.android.synthetic.main.fragment_order.*


class OrdersFragment : Fragment(R.layout.fragment_order) {

    lateinit var commonViewModel: CommonViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        getList()
    }

    private fun getList() {
        commonViewModel.getList(requireContext()).observe(viewLifecycleOwner, Observer {

            if (!it.error) {

                if (it.orderList != null && it.orderList?.size != 0) {

                    ordersList.show()
                    noOrders.hide()

                    ordersList.layoutManager = LinearLayoutManager(requireContext())
                    ordersList.adapter = OrderListAdapter(requireContext(), it.orderList)

                } else {
                    noOrders.show()
                    ordersList.hide()
                }

            }
        })
    }

}