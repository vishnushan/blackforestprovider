package com.bakery.vendorprovider.view.fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bakery.vendorprovider.R
import com.bakery.vendorprovider.models.AssignedOrderData
import com.bakery.vendorprovider.rxbus.RxBusNotification
import com.bakery.vendorprovider.utils.*
import com.bakery.vendorprovider.view.activity.OrderDetailsActivity
import com.bakery.vendorprovider.viewmodel.CommonViewModel
import com.google.android.gms.location.*
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_ride.*
import kotlinx.android.synthetic.main.get_order_content.*
import kotlinx.android.synthetic.main.home_content.*


class RideFragment : Fragment(R.layout.fragment_ride) {


    lateinit var commonViewModel: CommonViewModel
    lateinit var sharedHelper: SharedHelper
    var orderId = ""

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    private lateinit var disposable: Disposable
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        sharedHelper = SharedHelper((requireContext()))
        homeLoading.show()
        homeContent.hide()
        initValues()
        initListener()
    }

    fun checkLocationPermision(){
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        locationListner()
        askLocationPermission()
    }

    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
//                        if (myLat == 0.0 && myLng == 0.0)
//                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
//                    getLastKnownLocation()
                }
            }
        })
    }
    override fun onResume() {
        super.onResume()
        checkLocationPermision();
        getAssignedOrder()
    }

    private fun getAssignedOrder() {

        no_order_layout.hide()
        orderContent.hide()
        orderLoading.show()
        commonViewModel.getAssignedOrders(requireContext()).observe(viewLifecycleOwner, Observer {
            if (it.error) {
                no_order_layout.show()
                orderContent.hide()
                orderLoading.hide()
            } else {
                setValues(it.orderDetails)
            }
        })

    }

    private fun setValues(orderDetails: AssignedOrderData?) {

        if (orderDetails == null || orderDetails.orderId == 0) {
            no_order_layout.show()
            orderContent.hide()
            orderLoading.hide()
            return
        }

        no_order_layout.hide()
        orderContent.show()
        orderLoading.hide()

        restaurantName.text = orderDetails.storeName
        restaurantAddress.text = orderDetails.storeAddress
        customerName.text = orderDetails.userName
        customerAddress.text = orderDetails.userAddress
        orderNumber.text = orderDetails.orderRefferenceId
        totalAmount.text = orderDetails.amount
        orderId = orderDetails.orderId.toString()

        if (orderDetails.orderStatus == "assigned") {
            acceptRejectLayout.show()
        } else {
            acceptRejectLayout.hide()
        }
    }

    private fun initListener() {


        disposable = RxBusNotification.listen(String::class.java).doOnError {
        }.subscribe {
            requireActivity().runOnUiThread {
                getAssignedOrder()
            }
        }

        driver_name.text = sharedHelper.userName


        onlineOfflineSwitch.setOnClickListener {
            Handler().postDelayed({
                updateOnlineStatus(onlineOfflineSwitch.isChecked)
            }, 500)

        }

        orderContent.setOnClickListener {

            if (orderId != "" || orderId != "0") {
                var intent = Intent(requireContext(), OrderDetailsActivity::class.java)
                intent.putExtra("orderId", orderId)
                startActivity(intent)
            }

        }


        acceptOrder.setOnClickListener {
            no_order_layout.hide()
            orderContent.hide()
            orderLoading.show()
            commonViewModel.updateStatus(requireContext(), orderId, Constants.OrderStatus.ACCEPTED)
                .observe(viewLifecycleOwner,
                    Observer {
                        getAssignedOrder()
                    })
        }

        rejectOrder.setOnClickListener {

        }

    }

    private fun updateOnlineStatus(isOnline: Boolean) {
        homeLoading.show()
        homeContent.hide()

        commonViewModel.switchonlineStatus(requireContext(), isOnline).observe(viewLifecycleOwner,
            Observer {
                homeLoading.hide()
                homeContent.show()
                if (!it.error) {
                    setUserOnline(isOnline)
                }
            })
        if (isOnline) {
            getAssignedOrder()
        }


    }

    private fun initValues() {


        commonViewModel.getDashBoardData(requireContext()).observe(viewLifecycleOwner, Observer {

            homeLoading.hide()
            homeContent.show()

            if (!it.error) {

                it?.details?.let {

                    todayDeliveryAmount.text = it.todayEarning
                    todayDeliveryOrder.text = it.todayOrders + " orders"

                    weekDeliveryAmount.text = it.weekEarnings
                    weekDeliveryOrder.text = it.weekOrderd + " orders"

                    yesterdayDeliveryAmount.text = it.yesterdayEarnings
                    yesterdayDeliveryOrder.text = it.yesterdayOrders + " orders"

                    setUserOnline(it.isOnline ?: false)

                }

            }
        })
    }


    private fun setUserOnline(isOnline: Boolean) {

        if (isOnline) {
            contentBg.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.green_color
                )
            )
            onlineOfflineSwitch.isChecked = true

            onlineOfflineText.text = getString(R.string.online_text)
            onlineOfflineDescriText.text = getString(R.string.online_descri_text)
        } else {
            contentBg.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.text_color_dark_blue
                )
            )
            onlineOfflineSwitch.isChecked = false
            onlineOfflineText.text = getString(R.string.offline_text)
            onlineOfflineDescriText.text = getString(R.string.offline_descri_text)
        }
    }


}