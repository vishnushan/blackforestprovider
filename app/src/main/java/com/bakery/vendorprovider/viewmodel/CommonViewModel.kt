package com.bakery.vendorprovider.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bakery.vendorprovider.models.*
import com.bakery.vendorprovider.network.ApiInput
import com.bakery.vendorprovider.network.UrlHelper
import com.bakery.vendorprovider.repository.RegisterRepository
import com.bakery.vendorprovider.utils.Constants
import com.bakery.vendorprovider.utils.SharedHelper
import org.json.JSONObject

class CommonViewModel(application: Application) : AndroidViewModel(application) {


    private var sharedHelper = SharedHelper(application)
    private var repository = RegisterRepository.getInstance()

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
//        sharedHelper.let { header[Constants.ApiKeys.LANG] = "en" }
        sharedHelper.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

//    fun checkMobileexist(
//        context: Context,
//        mobileNumber: String
//    ): LiveData<CommonResponse> {
//
//        var jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)
//
//        return repository.checkMobileexist(
//            getApiParams(
//                context,
//                jsonObject,
//                UrlHelper.CHECKMOBILEEXIST
//            )
//        )
//    }

    fun getOtp(context: Context, mobileNumber: String): LiveData<CommonResponse> {
        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)

        return repository.getOtp(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.GET_OTP
            )
        )
    }

    fun getLogin(context: Context, mobileNumber: String): LiveData<CommonResponse> {
        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)

        return repository.getOtp(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.LOGIN
            )
        )
    }

    //
    fun verifyOtp(
        context: Context,
        otp: String,
        mobileNumber: String
    ): LiveData<VerifyOtpResponse> {
        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)
        jsonObject.put(Constants.ApiKeys.OTP, otp)


        return repository.verifyOtp(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.VERIFY_OTP
            )
        )

    }

    fun verifyOtpVendor(
        context: Context,
        otp: String,
        mobileNumber: String
    ): LiveData<VerifyOtpVendorResponse> {
        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)
        jsonObject.put(Constants.ApiKeys.OTP, otp)


        return repository.verifyOtpVendor(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.VERIFY_OTP_VENDOR
            )
        )

    }

    fun getDashBoardData(requireContext: Context): MutableLiveData<DashBoardResponse> {

        return repository.getDashBoardData(
            getApiParams(
                requireContext,
                null,
                UrlHelper.DASHBOARD
            )
        )
    }

    fun switchonlineStatus(
        requireContext: Context,
        online: Boolean
    ): MutableLiveData<CommonResponse> {

        var obj = JSONObject()
        obj.put("isOnline", online)

        return repository.switchonlineStatus(
            getApiParams(
                requireContext,
                obj,
                UrlHelper.UPDATEONLINESTATUS
            )
        )
    }

    fun getAssignedOrders(requireContext: Context): MutableLiveData<AssignedOrdersResponse> {


        return repository.getAssignedOrders(
            getApiParams(
                requireContext,
                null,
                UrlHelper.GETASSIGNORDERS
            )
        )
    }

    fun getOrderDetails(
        requireContext: Context,
        orderId: String
    ): MutableLiveData<OrderDetailsResponse> {

        var obj = JSONObject()
        obj.put("orderId", orderId)

        return repository.getOrderDetails(
            getApiParams(
                requireContext,
                obj,
                UrlHelper.DELIVERYORDERDETAILS
            )
        )
    }

    fun updateStatus(
        context: Context,
        orderId: String,
        status: String
    ): MutableLiveData<CommonResponse> {

        var obj = JSONObject()
        obj.put("orderId", orderId)
        obj.put("status", status)

        return repository.updateStatus(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEORDER_STATUS
            )
        )

    }

    fun getList(context: Context): MutableLiveData<OrderListResponse> {

        return repository.getList(
            getApiParams(
                context,
                null,
                UrlHelper.PASTORDERLIST
            )
        )
    }

    fun updateProfile(
        context: Context,
        name: String,
        email: String
    ): MutableLiveData<CommonResponse> {

        var obj = JSONObject()
        obj.put("email", email)
        obj.put("name", name)

        return repository.updateProfile(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEPROFILE
            )
        )
    }

    fun updateDeviceToken(context: Context) {

        var obj = JSONObject()
        obj.put("deviceToken", sharedHelper.firebaseToken)
        obj.put("outletId", sharedHelper.id)

        return repository.updateDeviceToken(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATE_DEVICE_TOKEN_VENDOR
            )
        )
    }

    fun getDetails(context: Context): MutableLiveData<VendorDetailsResponse> {

        var obj = JSONObject()
        obj.put("outletId", sharedHelper.id)

        return repository.getDetails(
            getApiParams(
                context,
                obj,
                UrlHelper.GETOUTLETDETAILS
            )
        )

    }

    fun updateVendorStatus(context: Context, status: Int): MutableLiveData<CommonResponse> {

        var obj = JSONObject()
        obj.put("outletId", sharedHelper.id)
        obj.put("status", status)

        return repository.updateVendorStatus(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATESTATUS
            )
        )


    }

    fun updateDeviceTokenDelivery(context: Context) {

        var obj = JSONObject()
        obj.put("deviceToken", sharedHelper.firebaseToken)

        return repository.updateDeviceTokenDelivery(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEDEVICETOKEN
            )
        )

    }

    fun getProductList(context: Context): MutableLiveData<ProductListResponse> {


        val obj = JSONObject()
        obj.put("outletId", sharedHelper.id)

        return repository.getProductList(
            getApiParams(
                context,
                obj,
                UrlHelper.VENDORPRODUCTLIST
            )
        )

    }

    fun updateProductStatus(
        context: Context,
        productId: Int,
        status: Int
    ): MutableLiveData<CommonResponse> {

        var obj = JSONObject()
        obj.put("productId", productId)
        obj.put("status", status)

        return repository.updateProductStatus(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEPRODUCTSTATUS
            )
        )

    }

    fun getProductDetails(context: Context, id: Int): MutableLiveData<ProductDetailResponse> {


        var obj = JSONObject()
        obj.put("productId", id)

        return repository.getProductDetails(
            getApiParams(
                context,
                obj,
                UrlHelper.PRODUCTDETAILS
            )
        )

    }

    fun updateProductDetails(
        context: Context,
        name: String,
        quantity: String,
        price: String,
        description: String,
        id: Int,
        status: Int
    ): MutableLiveData<CommonResponse> {

        var obj = JSONObject()
        obj.put("productId", id)
        obj.put("name", name)
        obj.put("quantity", quantity)
        obj.put("status", status)
        obj.put("price", price)
        obj.put("description", description)

        return repository.updateProductDetails(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEPRODUCTS
            )
        )

    }

    fun getOrders(context: Context): MutableLiveData<OrderListVendorResponse> {

        var obj = JSONObject()
        obj.put("outletId", sharedHelper.id)


        return repository.getOrders(
            getApiParams(
                context,
                obj,
                UrlHelper.ORDERLIST
            )
        )

    }

    fun updateOrderAccepted(context: Context, it: String?, s: String): MutableLiveData<CommonResponse> {


        var obj = JSONObject()
        obj.put("orderId", it)
        obj.put("status", s)


        return repository.updateOrderAccepted(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEPRDERSTATUS
            )
        )


    }

 fun updateOrderRejected(context: Context, it: String?, s: String): MutableLiveData<CommonResponse> {


        var obj = JSONObject()
        obj.put("orderId", it)
        obj.put("status", s)


        return repository.updateOrderRejected(
            getApiParams(
                context,
                obj,
                UrlHelper.UPDATEPRDERSTATUS
            )
        )


    }

//    fun registerUser(
//        context: Context,
//        name: String,
//        mobileNumber: String,
//        password: String
//    ): LiveData<LoginResponse> {
//        var jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)
//        jsonObject.put(Constants.ApiKeys.NAME, name)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//
//
//        return repository.registerUser(
//            getApiParams(
//                context,
//                jsonObject,
//                UrlHelper.USERSIGNUP
//            )
//        )
//
//    }
//
//    fun login(context: Context, mobile: String, password: String): LiveData<LoginResponse> {
//        var jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobile)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//        return repository.login(
//            getApiParams(
//                context,
//                jsonObject,
//                UrlHelper.LOGIN
//            )
//        )
//
//
//    }
//
//    fun changepassword(
//        context: Context,
//        password: String,
//        mobileNumber: String
//    ): LiveData<CommonResponse> {
//        var jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.MOBILE_NUMBER, mobileNumber)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//        return repository.changePassword(
//            getApiParams(
//                context,
//                jsonObject,
//                UrlHelper.CHANGEPASSWORD
//            )
//        )
//
//    }

}